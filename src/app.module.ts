import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';
import { CommandModule } from 'nestjs-command';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { CategoriesModule } from './categories/categories.module';
import { TransactionsModule } from './transactions/transactions.module';
import { BooksModule } from './books/books.module';
import { BookRequestsModule } from './book-requests/book-requests.module';
import { MeModule } from './me/me.module';
import { BookCommentsModule } from './book-comments/book-comments.module';

@Module({
  imports: [
    CommandModule,
    ConfigModule,
    TypeOrmModule.forRootAsync({
      useFactory: (configService: ConfigService) => {
        return configService.database as MysqlConnectionOptions;
      },
      inject: [ConfigService],
    }),
    UsersModule,
    AuthModule,
    CategoriesModule,
    TransactionsModule,
    BooksModule,
    BookRequestsModule,
    MeModule,
    BookCommentsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
