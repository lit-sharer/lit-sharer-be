import { Injectable, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { RegisterDto } from './dto/register.dto';
import { UsersService } from '../users/users.service';
import { LoginRequestDto, LoginResponseDto } from './dto/login.dto';
import { JwtPayload } from "./dto/jwt.dto";

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async register({ email, name, password, phoneNumber }: RegisterDto) {
    await this.usersService.create({ email, password, phoneNumber, name });
  }

  async login(data: LoginRequestDto): Promise<LoginResponseDto> {
    const user = await this.usersService.getByEmail(data.email);

    if (!user) {
      throw new NotFoundException('You have provided wrong credentials!');
    }

    if (!(await user.comparePassword(data.password))) {
      throw new NotFoundException('You have provided wrong credentials!');
    }

    return {
      token: await this.jwtService.signAsync({
        id: user.id,
        email: user.email,
        role: user.role,
      } as JwtPayload),
    };
  }
}
