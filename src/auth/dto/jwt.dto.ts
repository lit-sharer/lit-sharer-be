import { Roles } from '../../users/roles';

export interface JwtPayload {
  id: number;
  email: string;
  role: Roles;
}
