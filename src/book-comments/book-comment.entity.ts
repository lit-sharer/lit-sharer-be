import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { UserEntity } from '../users/user.entity';
import { BookEntity } from '../books/book.entity';

@Entity({ name: 'book_comments' })
export class BookCommentEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 2048 })
  comment: string;

  @Column()
  bookId: number;

  @Column()
  createdById: number;

  @Column()
  createdAt: Date;

  @Column()
  updatedAt: Date;

  @OneToOne(() => UserEntity)
  @JoinColumn({ name: 'created_by_id' })
  createdBy: UserEntity;

  @OneToOne(() => BookEntity)
  @JoinColumn({ name: 'book_id' })
  book: BookEntity;
}
