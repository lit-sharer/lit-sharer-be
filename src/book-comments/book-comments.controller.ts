import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiParam, ApiTags } from '@nestjs/swagger';

import { JwtAuthGuard } from '../auth/jwt.guard';
import { BookCommentsService } from './book-comments.service';
import { JwtPayload } from '../auth/dto/jwt.dto';
import { CreateCommentDto } from './dto/book-comments';

@Controller('books/:bookId/comments')
@ApiTags('Book Comments')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
export class BookCommentsController {
  constructor(private bookCommentsService: BookCommentsService) {}

  @Get('/')
  @ApiParam({ name: 'bookId' })
  list(@Param('bookId') bookId: number) {
    return this.bookCommentsService.list(bookId);
  }

  @Post('/')
  @HttpCode(HttpStatus.CREATED)
  async create(
    @Param('bookId') bookId: number,
    @Req() { user }: { user: JwtPayload },
    @Body() { comment }: CreateCommentDto,
  ) {
    await this.bookCommentsService.create({
      comment,
      bookId,
      createdById: user.id,
    });
  }

  @Delete('/:id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async delete(
    @Param('bookId') bookId: number,
    @Param('id') id: number,
    @Req() { user }: { user: JwtPayload },
  ) {
    await this.bookCommentsService.delete(bookId, id, user.id);
  }
}
