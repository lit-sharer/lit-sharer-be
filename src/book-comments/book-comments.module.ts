import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { BookCommentsService } from './book-comments.service';
import { BookCommentsController } from './book-comments.controller';
import { AuthModule } from '../auth/auth.module';
import { BookCommentEntity } from './book-comment.entity';
import { CommentsController } from './comments.controller';

@Module({
  imports: [TypeOrmModule.forFeature([BookCommentEntity]), AuthModule],
  providers: [BookCommentsService],
  controllers: [BookCommentsController, CommentsController],
  exports: [TypeOrmModule],
})
export class BookCommentsModule {}
