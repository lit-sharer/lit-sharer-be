import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { BookCommentEntity } from './book-comment.entity';
import { CreateCommentDto } from './dto/book-comments';

@Injectable()
export class BookCommentsService {
  constructor(
    @InjectRepository(BookCommentEntity)
    private repo: Repository<BookCommentEntity>,
  ) {}

  list(bookId: number): Promise<BookCommentEntity[]> {
    return this.repo.find({
      where: {
        bookId,
      },
      relations: ['book', 'createdBy'],
      order: {
        createdAt: 'DESC',
      },
    });
  }

  getByUser(createdById: number): Promise<BookCommentEntity[]> {
    return this.repo.find({
      where: {
        createdById,
      },
      relations: ['book', 'createdBy'],
      order: {
        createdAt: 'DESC',
      },
    });
  }

  create({ comment, bookId, createdById }: CreateCommentDto) {
    const entity = this.repo.create({
      bookId,
      comment,
      createdById,
    });

    return this.repo.insert(entity);
  }

  async delete(bookId: number, id: number, createdById: number) {
    const comment = await this.repo.findOne({
      where: {
        id,
        bookId,
        createdById,
      },
    });

    if (!comment) {
      throw new NotFoundException('Comment not found!');
    }

    return this.repo.delete({ id });
  }
}
