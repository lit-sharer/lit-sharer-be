import { Controller, Get, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

import { JwtAuthGuard } from '../auth/jwt.guard';
import { BookCommentsService } from './book-comments.service';
import { JwtPayload } from '../auth/dto/jwt.dto';

@Controller('comments')
@ApiTags('My Comments')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
export class CommentsController {
  constructor(private service: BookCommentsService) {}

  @Get('/')
  getMyComments(@Req() { user }: { user: JwtPayload }) {
    return this.service.getByUser(user.id);
  }
}
