import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { BookRequestStatuses } from './book-requests.enum';
import { UserEntity } from '../users/user.entity';
import { BookEntity } from '../books/book.entity';

@Entity({ name: 'book_requests' })
export class BookRequestEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true, length: 2048 })
  notes: string;

  @Column()
  bookId: number;

  @Column()
  userId: number;

  @Column()
  status: BookRequestStatuses;

  @Column()
  createdAt: Date;

  @Column()
  updatedAt: Date;

  @OneToOne(() => UserEntity)
  @JoinColumn()
  user: UserEntity;

  @OneToOne(() => BookEntity)
  @JoinColumn()
  book: BookEntity;
}
