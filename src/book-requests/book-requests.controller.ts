import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiParam, ApiTags } from '@nestjs/swagger';

import { BookRequestsService } from './book-requests.service';
import { JwtPayload } from '../auth/dto/jwt.dto';
import { CreateBookRequestDto } from './dto/book-requests';
import { JwtAuthGuard } from '../auth/jwt.guard';

@Controller('books/:bookId/requests')
@ApiTags('Book Requests')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
export class BookRequestsController {
  constructor(private bookRequestsService: BookRequestsService) {}

  @Get('/')
  @ApiParam({ name: 'bookId', type: 'string' })
  async list(@Param('bookId') bookId: number) {
    return await this.bookRequestsService.list(bookId);
  }

  @Post('/')
  @HttpCode(HttpStatus.CREATED)
  @ApiParam({ name: 'bookId', type: 'string' })
  async create(
    @Param('bookId') bookId: number,
    @Body() data: CreateBookRequestDto,
    @Req() { user }: { user: JwtPayload },
  ) {
    await this.bookRequestsService.create(data, bookId, user.id);

    return;
  }

  @Patch('/:id/reject')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiParam({ name: 'bookId', type: 'string' })
  @ApiParam({ name: 'id', type: 'string' })
  async reject(
    @Param('bookId') bookId: number,
    @Param('id') id: number,
    @Req() { user }: { user: JwtPayload },
  ) {
    await this.bookRequestsService.reject(bookId, id, user.id);

    return;
  }

  @Patch('/:id/resolve')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiParam({ name: 'bookId', type: 'string' })
  @ApiParam({ name: 'id', type: 'string' })
  async resolve(
    @Param('bookId') bookId: number,
    @Param('id') id: number,
    @Req() { user }: { user: JwtPayload },
  ) {
    await this.bookRequestsService.resolve(bookId, id, user.id);

    return;
  }

  @Delete('/:id')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiParam({ name: 'bookId', type: 'string' })
  @ApiParam({ name: 'id', type: 'string' })
  async delete(
    @Param('bookId') bookId: number,
    @Param('id') id: number,
    @Req() { user }: { user: JwtPayload },
  ) {
    await this.bookRequestsService.delete(bookId, id, user.id);

    return;
  }
}
