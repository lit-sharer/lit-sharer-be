export enum BookRequestStatuses {
  Pending,
  Resolved,
  Rejected,
}
