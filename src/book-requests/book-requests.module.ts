import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { BookRequestsService } from './book-requests.service';
import { BookRequestsController } from './book-requests.controller';
import { BookRequestEntity } from './book-request.entity';
import { AuthModule } from '../auth/auth.module';
import { RequestsController } from './requests.controller';
import { BooksModule } from '../books/books.module';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([BookRequestEntity]),
    AuthModule,
    BooksModule,
    UsersModule,
  ],
  providers: [BookRequestsService],
  controllers: [BookRequestsController, RequestsController],
  exports: [TypeOrmModule],
})
export class BookRequestsModule {}
