import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { BookRequestEntity } from './book-request.entity';
import { CreateBookRequestDto } from './dto/book-requests';
import { BookRequestStatuses } from './book-requests.enum';
import { WalletsService } from '../users/wallets/wallets.service';
import { BooksService } from '../books/books.service';

@Injectable()
export class BookRequestsService {
  constructor(
    @InjectRepository(BookRequestEntity)
    private bookRequestsRepo: Repository<BookRequestEntity>,
    private booksService: BooksService,
    private walletsService: WalletsService,
  ) {}

  list(bookId: number) {
    return this.bookRequestsRepo.find({
      where: { bookId },
      relations: ['user', 'user.address'],
      order: {
        createdAt: 'DESC',
      },
    });
  }

  listByUser(userId: number) {
    return this.bookRequestsRepo.find({
      where: { userId },
      relations: ['user', 'book'],
      order: {
        createdAt: 'DESC',
      },
    });
  }

  listToReview(userId: number) {
    return this.bookRequestsRepo
      .createQueryBuilder('requests')
      .innerJoinAndSelect('requests.book', 'book')
      .innerJoinAndSelect('requests.user', 'user')
      .leftJoinAndSelect('user.address', 'address')
      .where('book.createdById = :createdById', { createdById: userId })
      .orderBy('requests.createdAt', 'DESC')
      .getMany();
  }

  async create(dto: CreateBookRequestDto, bookId: number, userId: number) {
    const existed = await this.bookRequestsRepo.findOne({
      bookId,
      userId,
      status: BookRequestStatuses.Pending,
    });

    if (existed) {
      throw new ForbiddenException(
        'You are not allowed to make request twice!',
      );
    }

    const book = await this.booksService.get(bookId);

    await this.walletsService.containsAmount(userId, book.price);

    const entity = this.bookRequestsRepo.create({
      notes: dto.notes,
      bookId,
      userId,
      status: BookRequestStatuses.Pending,
    });

    await this.walletsService.blockAmount(userId, book.price);

    return this.bookRequestsRepo.insert(entity);
  }

  async reject(bookId: number, id: number, userId: number) {
    const request = await this.bookRequestsRepo.findOne({ id, bookId });

    if (!request) {
      throw new NotFoundException('Request not found!');
    }

    if (request.status !== BookRequestStatuses.Pending) {
      throw new ForbiddenException(
        'You are allowed to reject only a request with Pending status!',
      );
    }

    const book = await this.booksService.get(bookId);

    if (userId !== book.createdById) {
      throw new ForbiddenException(
        'You are not allowed to reject this request!',
      );
    }

    await this.walletsService.unblockAmount(request.userId, book.price);

    return this.bookRequestsRepo.update(
      {
        id,
        bookId,
      },
      {
        status: BookRequestStatuses.Rejected,
      },
    );
  }

  async resolve(bookId: number, id: number, userId: number) {
    const request = await this.bookRequestsRepo.findOne({ id, bookId });

    if (!request) {
      throw new NotFoundException('Request not found!');
    }

    if (request.status !== BookRequestStatuses.Pending) {
      throw new ForbiddenException(
        'You are allowed to resolve only a request with Pending status!',
      );
    }

    const book = await this.booksService.get(bookId);

    if (userId !== book.createdById) {
      throw new ForbiddenException(
        'You are not allowed to resolve this request!',
      );
    }

    await this.walletsService.chargeBlocked(
      request.userId,
      book.price,
      `Payment for resolved request #${id} (Book #${bookId})`,
    );

    await this.walletsService.credit(
      book.createdById,
      book.price,
      `Payment for resolved request #${id} (Book #${bookId})`,
    );

    return this.bookRequestsRepo.update(
      {
        id,
        bookId,
      },
      {
        status: BookRequestStatuses.Resolved,
      },
    );
  }

  async delete(bookId: number, id: number, userId: number) {
    const request = await this.bookRequestsRepo.findOne({ id, bookId, userId });

    if (!request) {
      throw new NotFoundException('Request not found!');
    }

    if (request.status !== BookRequestStatuses.Pending) {
      throw new ForbiddenException(
        'You are allowed to delete only a request with Pending status!',
      );
    }

    const book = await this.booksService.get(bookId);

    await this.walletsService.unblockAmount(userId, book.price);

    return this.bookRequestsRepo.delete({
      id,
      bookId,
      userId,
    });
  }
}
