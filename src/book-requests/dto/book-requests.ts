import { IsString } from 'class-validator';

export class CreateBookRequestDto {
  @IsString()
  notes: string;
}
