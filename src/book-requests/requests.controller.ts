import { Controller, Get, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

import { BookRequestsService } from './book-requests.service';
import { JwtAuthGuard } from '../auth/jwt.guard';
import { JwtPayload } from '../auth/dto/jwt.dto';

@Controller('requests')
@ApiTags('My Requests')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
export class RequestsController {
  constructor(private bookRequestsService: BookRequestsService) {}

  @Get('/my')
  async listMy(@Req() { user }: { user: JwtPayload }) {
    return this.bookRequestsService.listByUser(user.id);
  }

  @Get('/review')
  async listToReview(@Req() { user }: { user: JwtPayload }) {
    return this.bookRequestsService.listToReview(user.id);
  }
}
