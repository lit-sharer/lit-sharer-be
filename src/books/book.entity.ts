import {
  AfterLoad,
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { BookStatuses } from './books.enum';
import { UserEntity } from '../users/user.entity';
import { CategoryEntity } from '../categories/category.entity';
import { BookRequestEntity } from '../book-requests/book-request.entity';
import { getFileUrl } from '../core/utils/files';

@Entity({ name: 'books' })
export class BookEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false, length: 512 })
  name: string;

  @Column({ nullable: true, length: 2048 })
  description: string;

  @Column()
  pagesCount: number;

  @Column()
  categoryId: number;

  @Column()
  price: number;

  @Column()
  views: number;

  @Column()
  status: BookStatuses;

  @Column({ nullable: true, length: 2048 })
  mainPhoto: string;

  @Column({ nullable: true, length: 256 })
  authors: string;

  @Column()
  createdById: number;

  @Column()
  createdAt: Date;

  @Column()
  updatedAt: Date;

  @OneToOne(() => UserEntity)
  @JoinColumn({ name: 'created_by_id' })
  createdBy: UserEntity;

  @OneToOne(() => CategoryEntity)
  @JoinColumn()
  category: CategoryEntity;

  @OneToMany(() => BookRequestEntity, (request) => request.book)
  @JoinColumn()
  public requests: BookRequestEntity[];

  // readonly url
  protected mainPhotoUrl: string;

  @AfterLoad()
  getUrl() {
    this.mainPhotoUrl = this.mainPhoto ? getFileUrl(this.mainPhoto) : null;
  }
}
