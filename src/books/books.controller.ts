import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query,
  Req, UploadedFile,
  UseGuards,
  UseInterceptors
} from "@nestjs/common";
import { ApiBearerAuth, ApiQuery, ApiTags } from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';

import { BookEntity } from './book.entity';
import { BooksService } from './books.service';
import { JwtAuthGuard } from '../auth/jwt.guard';
import { CreateBookDto, UpdateBookDto } from './dto/books';
import { JwtPayload } from '../auth/dto/jwt.dto';

@Controller('books')
@ApiTags('Books')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
export class BooksController {
  constructor(private booksService: BooksService) {}

  @Get('/my')
  list(@Req() { user }: { user: JwtPayload }): Promise<BookEntity[]> {
    return this.booksService.list({
      createdById: user.id,
    });
  }

  @Get('/search')
  @ApiQuery({ name: 'query' })
  search(@Query('query') query: string): Promise<BookEntity[]> {
    return this.booksService.search(query);
  }

  @Get('/:id')
  show(@Param('id') id: number): Promise<BookEntity> {
    return this.booksService.get(id);
  }

  @Post('/')
  @UseInterceptors(FileInterceptor('file'))
  @HttpCode(HttpStatus.CREATED)
  async create(
    @Body() data: CreateBookDto,
    @Req() { user }: { user: JwtPayload },
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      data.mainPhoto = file.filename;
    }
    await this.booksService.create(data, user.id);
  }

  @Patch('/:id')
  @UseInterceptors(FileInterceptor('file'))
  @HttpCode(HttpStatus.NO_CONTENT)
  async update(
    @Param('id') id: number,
    @Body() data: UpdateBookDto,
    @Req() { user }: { user: JwtPayload },
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      console.log(file);
      data.mainPhoto = file.filename;
    }
    await this.booksService.update(id, data, user.id);
  }

  @Patch('/:id/activate')
  @HttpCode(HttpStatus.NO_CONTENT)
  async activate(@Param('id') id: number) {
    await this.booksService.activate(id);
  }

  @Patch('/:id/block')
  @HttpCode(HttpStatus.NO_CONTENT)
  async block(@Param('id') id: number) {
    await this.booksService.block(id);
  }

  @Delete('/:id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async delete(@Param('id') id: number, @Req() { user }: { user: JwtPayload }) {
    await this.booksService.delete(id, user.id);
  }
}
