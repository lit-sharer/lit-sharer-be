export enum BookStatuses {
  Draft,
  Active,
  Blocked,
}
