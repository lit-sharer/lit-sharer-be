import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { BooksService } from './books.service';
import { BooksController } from './books.controller';
import { BookEntity } from './book.entity';
import { AuthModule } from '../auth/auth.module';
import { FilesModule } from '../core/files/files.module';

@Module({
  imports: [TypeOrmModule.forFeature([BookEntity]), AuthModule, FilesModule],
  providers: [BooksService],
  controllers: [BooksController],
  exports: [TypeOrmModule, BooksService],
})
export class BooksModule {}
