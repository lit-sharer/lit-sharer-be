import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindConditions, Like, Repository } from 'typeorm';

import { BookEntity } from './book.entity';
import { BookStatuses } from './books.enum';
import { CreateBookDto, UpdateBookDto } from './dto/books';
import { sanitizeObject } from '../core/utils/sanitize';

@Injectable()
export class BooksService {
  constructor(
    @InjectRepository(BookEntity) private booksRepo: Repository<BookEntity>,
  ) {}

  list(params: FindConditions<BookEntity> = {}) {
    return this.booksRepo.find({
      where: {
        ...params,
      },
      relations: ['createdBy', 'category'],
      order: {
        status: 'ASC',
        createdAt: 'DESC',
      },
    });
  }

  search(query: string) {
    return this.booksRepo.find({
      where: [
        { name: Like(`%${query}%`), status: BookStatuses.Active },
        { description: Like(`%${query}%`), status: BookStatuses.Active },
        { authors: Like(`%${query}%`), status: BookStatuses.Active },
      ],
      take: 10,
    });
  }

  async get(id: number) {
    const entity = await this.booksRepo.findOne({
      where: { id },
      relations: ['createdBy', 'category'],
    });

    if (!entity) {
      throw new NotFoundException('Book not found!');
    }

    return entity;
  }

  create(dto: CreateBookDto, userId: number) {
    const entity = this.booksRepo.create({
      name: dto.name,
      pagesCount: dto.pagesCount,
      price: dto.price,
      categoryId: dto.categoryId,
      description: dto.description,
      authors: dto.authors,
      status: BookStatuses.Draft,
      createdById: userId,
      mainPhoto: dto.mainPhoto,
    });

    return this.booksRepo.insert(entity);
  }

  async update(id: number, dto: UpdateBookDto, userId: number) {
    const entity = await this.get(id);

    if (entity.createdById !== userId) {
      throw new ForbiddenException('You are not allowed to update this book!');
    }

    return this.booksRepo.update(
      { id: entity.id },
      sanitizeObject({
        name: dto.name,
        pagesCount: dto.pagesCount,
        price: dto.price,
        description: dto.description,
        authors: dto.authors,
        categoryId: dto.categoryId,
        mainPhoto: dto.mainPhoto,
      }),
    );
  }

  activate(id: number) {
    return this.booksRepo.update({ id }, { status: BookStatuses.Active });
  }

  block(id: number) {
    return this.booksRepo.update({ id }, { status: BookStatuses.Blocked });
  }

  async delete(id: number, userId: number) {
    const entity = await this.get(id);

    if (entity.createdById !== userId) {
      throw new ForbiddenException('You are not allowed to delete this book!');
    }

    return this.booksRepo.delete({ id: entity.id });
  }
}
