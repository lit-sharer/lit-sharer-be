import { IsNumber, IsOptional, IsString, Min } from 'class-validator';
import { Transform } from 'class-transformer';

export class CreateBookDto {
  @IsString()
  name: string;

  @IsString()
  @IsOptional()
  description: string;

  @IsString()
  @IsOptional()
  authors: string;

  @IsNumber()
  @IsOptional()
  @Transform(({ value }) => Number(value), { toClassOnly: true })
  pagesCount: number;

  @IsNumber()
  @Min(0)
  @Transform(({ value }) => Number(value), { toClassOnly: true })
  price: number;

  @IsNumber()
  @Transform(({ value }) => Number(value), { toClassOnly: true })
  categoryId: number;

  @IsOptional()
  mainPhoto: string;
}

export class UpdateBookDto {
  @IsString()
  @IsOptional()
  name: string;

  @IsString()
  @IsOptional()
  description: string;

  @IsString()
  @IsOptional()
  authors: string;

  @IsNumber()
  @IsOptional()
  @Transform(({ value }) => Number(value), { toClassOnly: true })
  pagesCount: number;

  @IsNumber()
  @Min(0)
  @IsOptional()
  @Transform(({ value }) => Number(value), { toClassOnly: true })
  price: number;

  @IsNumber()
  @IsOptional()
  @Transform(({ value }) => Number(value), { toClassOnly: true })
  categoryId: number;

  @IsOptional()
  mainPhoto: string;
}
