import { Controller, Get, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

import { CategoryEntity } from './category.entity';
import { CategoriesService } from './categories.service';
import { JwtAuthGuard } from '../auth/jwt.guard';

@Controller('categories')
@ApiTags('Categories')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
export class CategoriesController {
  constructor(private categoriesService: CategoriesService) {}

  @Get('/')
  list(): Promise<CategoryEntity[]> {
    return this.categoriesService.list();
  }
}
