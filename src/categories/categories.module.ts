import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CategoriesService } from './categories.service';
import { CategoriesController } from './categories.controller';
import { CategoryEntity } from './category.entity';
import { CategoriesSeeder } from './seeds/categories.seeder';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    AuthModule,
    TypeOrmModule.forFeature([CategoryEntity]),
  ],
  providers: [CategoriesService, CategoriesSeeder],
  controllers: [CategoriesController],
  exports: [CategoriesService, CategoriesSeeder, TypeOrmModule],
})
export class CategoriesModule {}
