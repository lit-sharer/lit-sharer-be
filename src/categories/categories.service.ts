import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { CreateCategoryDto } from './dto/categories';
import { CategoryEntity } from './category.entity';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(CategoryEntity)
    private categoriesRepo: Repository<CategoryEntity>,
  ) {}

  list(): Promise<CategoryEntity[]> {
    return this.categoriesRepo.find();
  }

  create(data: CreateCategoryDto | CreateCategoryDto[]) {
    return this.categoriesRepo.insert(data);
  }
}
