import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'categories' })
export class CategoryEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false, length: 128 })
  name: string;

  @Column({ nullable: true, length: 128 })
  icon: string;
}
