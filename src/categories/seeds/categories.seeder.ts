import { Injectable } from '@nestjs/common';
import { Command } from 'nestjs-command';

import { CategoriesService } from '../categories.service';
import { CreateCategoryDto } from '../dto/categories';

@Injectable()
export class CategoriesSeeder {
  CATEGORIES = [
    { name: 'JavaScript' },
    { name: 'C#' },
    { name: 'Java' },
    { name: 'Python' },
    { name: 'QA&QC' },
    { name: 'C++' },
    { name: 'C' },
    { name: 'Testing Fundamentals' },
  ] as CreateCategoryDto[];

  constructor(private categoriesService: CategoriesService) {}

  @Command({
    command: 'seed:categories',
    describe: 'seeds categories',
  })
  async seed() {
    await this.categoriesService.create(this.CATEGORIES);

    console.log('Categories seeding process has ended successfully!');
  }
}
