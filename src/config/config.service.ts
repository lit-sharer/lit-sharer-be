import { Injectable } from '@nestjs/common';
import * as dotenv from 'dotenv';

dotenv.config();

import database from './database';

@Injectable()
export class ConfigService {
  public database = { ...database, migrations: [] };
}
