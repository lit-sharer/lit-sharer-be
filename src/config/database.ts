import { SnakeNamingStrategy } from 'typeorm-snake-naming-strategy';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';

export default {
  type: 'mysql',
  host: process.env.APP_DATABASE_HOST,
  port: Number(process.env.APP_DATABASE_PORT),
  username: process.env.APP_DATABASE_USER,
  password: process.env.APP_DATABASE_PASSWORD,
  database: process.env.APP_DATABASE_NAME,
  namingStrategy: new SnakeNamingStrategy(),
  synchronize: false,
  migrations: ['src/migrations/**.ts'],
  migrationsRun: false,
  autoLoadEntities: true,
  cli: {
    migrationsDir: 'src/migrations',
  },
} as MysqlConnectionOptions;
