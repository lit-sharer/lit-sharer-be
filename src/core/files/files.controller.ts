import { Controller, Get, HttpStatus, Param, Res } from '@nestjs/common';

@Controller('files')
export class FilesController {
  @Get(':imagename')
  getImage(@Param('imagename') image, @Res() res) {
    const response = res.sendFile(image, { root: './upload' });
    return {
      status: HttpStatus.OK,
      data: response,
    };
  }
}
