import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';

import { FilesController } from './files.controller';
import { editFileName } from '../utils/files';

@Module({
  imports: [
    MulterModule.register({
      storage: diskStorage({
        destination: './upload',
        filename: editFileName,
      }),
    }),
  ],
  controllers: [FilesController],
  exports: [MulterModule],
})
export class FilesModule {}
