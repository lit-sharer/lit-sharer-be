export const sanitizeObject = (obj) =>
  Object.entries(obj).reduce((acc, [key, value]) => {
    if (value === undefined) {
      return acc;
    }

    return {
      ...acc,
      [key]: value,
    };
  }, {});
