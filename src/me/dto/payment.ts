import { IsNumber, Min } from 'class-validator';
import { Transform } from 'class-transformer';

export class CreditAccountDto {
  @IsNumber()
  @Min(100)
  @Transform(({ value }) => Number(value))
  amount: number;
}
