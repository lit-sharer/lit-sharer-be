import {
  Body,
  Controller,
  Get,
  Patch,
  Post,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';

import { JwtAuthGuard } from '../auth/jwt.guard';
import { JwtPayload } from '../auth/dto/jwt.dto';
import { MeService } from './me.service';
import { UpdateProfileDto } from './dto/profile';
import { ChangePasswordDto } from './dto/password';
import { UpsertAddressDto } from '../users/dto/addresses';
import { CreditAccountDto } from './dto/payment';

@Controller('me')
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@ApiTags('Profile')
export class MeController {
  constructor(private meService: MeService) {}

  @Get('/')
  getMe(@Req() { user }: { user: JwtPayload }) {
    return this.meService.getMe(user.id);
  }

  @Patch('/')
  @UseInterceptors(FileInterceptor('file'))
  async updateProfile(
    @Req() { user }: { user: JwtPayload },
    @Body() dto: UpdateProfileDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      dto.avatar = file.filename;
    }
    await this.meService.update(user.id, dto);
  }

  @Patch('/password')
  async updatePassword(
    @Req() { user }: { user: JwtPayload },
    @Body() dto: ChangePasswordDto,
  ) {
    await this.meService.updatePassword(user.id, dto);
  }

  @Post('/address')
  async upsertAddress(
    @Req() { user }: { user: JwtPayload },
    @Body() dto: UpsertAddressDto,
  ) {
    await this.meService.upsertAddress(user.id, dto);
  }

  @Post('/credit')
  async creditBalance(
    @Req() { user }: { user: JwtPayload },
    @Body() dto: CreditAccountDto,
  ) {
    await this.meService.creditBalance(user.id, dto);
  }
}
