import { Module } from '@nestjs/common';

import { MeController } from './me.controller';
import { MeService } from './me.service';
import { AuthModule } from '../auth/auth.module';
import { UsersModule } from '../users/users.module';
import { FilesModule } from '../core/files/files.module';

@Module({
  imports: [AuthModule, UsersModule, FilesModule],
  controllers: [MeController],
  providers: [MeService],
})
export class MeModule {}
