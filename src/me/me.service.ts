import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { UpdateProfileDto } from './dto/profile';
import { ChangePasswordDto } from './dto/password';
import { UpsertAddressDto } from '../users/dto/addresses';
import { AddressesService } from '../users/addresses/addresses.service';
import { CreditAccountDto } from './dto/payment';
import { WalletsService } from '../users/wallets/wallets.service';
import { sanitizeObject } from '../core/utils/sanitize';

@Injectable()
export class MeService {
  constructor(
    private usersService: UsersService,
    private walletsService: WalletsService,
    private addressesService: AddressesService,
  ) {}

  getMe(id: number) {
    return this.usersService.getById(id);
  }

  async update(
    id: number,
    { email, name, phoneNumber, avatar }: UpdateProfileDto,
  ) {
    if (email) {
      const entity = await this.usersService.getByEmail(email);

      if (entity && entity.id !== id) {
        throw new ForbiddenException('This email already exists!');
      }
    }

    return this.usersService.update(
      id,
      sanitizeObject({
        email,
        name,
        phoneNumber,
        avatar,
      }),
    );
  }

  async updatePassword(
    id: number,
    { oldPassword, newPassword }: ChangePasswordDto,
  ) {
    const user = await this.usersService.get({
      where: { id },
      select: ['id', 'passwordHash'],
    });

    if (!user) {
      throw new NotFoundException('User not found!');
    }

    const match = await user.comparePassword(oldPassword);

    if (!match) {
      throw new ForbiddenException('Your old password is incorrect!');
    }

    user.passwordHash = newPassword;

    return this.usersService.save(user);
  }

  upsertAddress(id: number, dto: UpsertAddressDto) {
    return this.addressesService.upsert(id, dto);
  }

  creditBalance(userId: number, dto: CreditAccountDto) {
    return this.walletsService.creditByCard(userId, dto.amount);
  }
}
