import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class AddedBooks1618686058661 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'books',
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'name',
            type: 'varchar',
            length: '512',
          },
          {
            name: 'description',
            type: 'varchar',
            isNullable: true,
            length: '2048',
          },
          {
            name: 'pages_count',
            type: 'int',
            isNullable: true,
          },
          {
            name: 'category_id',
            type: 'int',
          },
          {
            name: 'price',
            type: 'decimal',
          },
          {
            name: 'views',
            type: 'int',
            default: 0,
          },
          {
            name: 'status',
            type: 'int',
          },
          {
            name: 'main_photo',
            type: 'varchar',
            isNullable: true,
            length: '2048',
          },
          {
            name: 'authors',
            type: 'varchar',
            isNullable: true,
            length: '256',
          },
          {
            name: 'created_by_id',
            type: 'int',
          },
          {
            name: 'created_at',
            type: 'datetime(3)',
            default: 'CURRENT_TIMESTAMP(3)',
          },
          {
            name: 'updated_at',
            type: 'datetime(3)',
            default: 'CURRENT_TIMESTAMP(3)',
            onUpdate: 'CURRENT_TIMESTAMP(3)',
          },
        ],
        foreignKeys: [
          {
            name: 'FK_users_books_created_by_id',
            referencedTableName: 'users',
            referencedColumnNames: ['id'],
            columnNames: ['created_by_id'],
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
          },
          {
            name: 'FK_categories_books_category_id',
            referencedTableName: 'categories',
            referencedColumnNames: ['id'],
            columnNames: ['category_id'],
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('books');
  }
}
