import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class AddedBookComments1619680829416 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'book_comments',
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'comment',
            type: 'varchar',
            length: '2048',
          },
          {
            name: 'book_id',
            type: 'int',
          },
          {
            name: 'created_by_id',
            type: 'int',
          },
          {
            name: 'created_at',
            type: 'datetime(3)',
            default: 'CURRENT_TIMESTAMP(3)',
          },
          {
            name: 'updated_at',
            type: 'datetime(3)',
            default: 'CURRENT_TIMESTAMP(3)',
            onUpdate: 'CURRENT_TIMESTAMP(3)',
          },
        ],
        foreignKeys: [
          {
            name: 'FK_users_book_comments_created_by_id',
            referencedTableName: 'users',
            referencedColumnNames: ['id'],
            columnNames: ['created_by_id'],
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
          },
          {
            name: 'FK_books_book_comments_book_id',
            referencedTableName: 'books',
            referencedColumnNames: ['id'],
            columnNames: ['book_id'],
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('book_comments');
  }
}
