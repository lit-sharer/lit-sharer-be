import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddedBlockedBalance1619871284975 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'wallets',
      new TableColumn({
        name: 'blocked_balance',
        type: 'decimal',
        default: 0,
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('wallets', 'blocked_balance');
  }
}
