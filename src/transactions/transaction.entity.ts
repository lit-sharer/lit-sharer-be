import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

import {
  TransactionInitiators,
  TransactionPurposes,
  TransactionTypes,
} from './transactions.enum';
import { UserEntity } from '../users/user.entity';

@Entity({ name: 'transactions' })
export class TransactionEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  type: TransactionTypes;

  @Column()
  initiator: TransactionInitiators;

  @Column()
  purpose: TransactionPurposes;

  @Column()
  comment: string;

  @Column()
  createdAt: Date;

  @Column()
  amount: number;

  @Column()
  userId: number;

  @OneToOne(() => UserEntity, (user: UserEntity) => user.transactions)
  public user: UserEntity;
}
