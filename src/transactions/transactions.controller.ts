import { Controller, Get, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

import { TransactionsService } from './transactions.service';
import { JwtPayload } from '../auth/dto/jwt.dto';
import { JwtAuthGuard } from '../auth/jwt.guard';

@Controller('transactions')
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@ApiTags('My Transactions')
export class TransactionsController {
  constructor(private transactionsService: TransactionsService) {}

  @Get('/')
  list(@Req() { user }: { user: JwtPayload }) {
    return this.transactionsService.listByUser(user.id);
  }
}
