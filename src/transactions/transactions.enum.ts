export enum TransactionTypes {
  Credit,
  Charge,
}

export enum TransactionPurposes {
  Purchase,
  Refund,
  Cancel,
  Other,
}

export enum TransactionInitiators {
  System,
  User,
}
