import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { TransactionEntity } from './transaction.entity';
import {
  TransactionInitiators,
  TransactionPurposes,
  TransactionTypes,
} from './transactions.enum';

@Injectable()
export class TransactionsService {
  constructor(
    @InjectRepository(TransactionEntity)
    private transactionsRepo: Repository<TransactionEntity>,
  ) {}

  listByUser(userId: number) {
    return this.transactionsRepo.find({
      where: {
        userId,
      },
      order: {
        createdAt: 'DESC',
      },
    });
  }

  async createCreditBySystem(userId: number, amount: number, comment?: string) {
    const entity = this.transactionsRepo.create({
      userId,
      amount,
      type: TransactionTypes.Credit,
      initiator: TransactionInitiators.System,
      purpose: TransactionPurposes.Other,
      comment: comment || 'Balance was increased by system',
    });

    await this.transactionsRepo.insert(entity);
  }

  async createCreditByUser(userId: number, amount: number, comment: string) {
    const entity = this.transactionsRepo.create({
      userId,
      amount,
      type: TransactionTypes.Credit,
      initiator: TransactionInitiators.User,
      purpose: TransactionPurposes.Other,
      comment: comment,
    });

    await this.transactionsRepo.insert(entity);
  }

  async createChargeBySystem(userId: number, amount: number, comment?: string) {
    const entity = this.transactionsRepo.create({
      userId,
      amount,
      type: TransactionTypes.Charge,
      initiator: TransactionInitiators.System,
      purpose: TransactionPurposes.Purchase,
      comment: comment || 'Balance was decreased by system',
    });

    await this.transactionsRepo.insert(entity);
  }
}
