import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { UserEntity } from '../user.entity';

@Entity({ name: 'user_addresses' })
export class AddressEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  country: string;

  @Column()
  city: string;

  @Column()
  address1: string;

  @Column()
  address2: string;

  @Column()
  zip: number;

  @Column()
  userId: number;

  @OneToOne(() => UserEntity)
  @JoinColumn()
  public user: UserEntity;
}
