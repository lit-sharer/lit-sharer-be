import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { AddressEntity } from './address.entity';
import { UpsertAddressDto } from '../dto/addresses';
import { sanitizeObject } from '../../core/utils/sanitize';

@Injectable()
export class AddressesService {
  constructor(
    @InjectRepository(AddressEntity) private repo: Repository<AddressEntity>,
  ) {}

  get(userId: number) {
    return this.repo.findOne({ userId });
  }

  async upsert(userId: number, dto: UpsertAddressDto) {
    const address = await this.get(userId);

    if (address) {
      return this.repo.update(
        { id: address.id },
        sanitizeObject({
          address1: dto.address1,
          address2: dto.address2,
          city: dto.city,
          country: dto.country,
          zip: dto.zip,
        }),
      );
    }

    return this.repo.insert({
      address1: dto.address1,
      address2: dto.address2,
      city: dto.city,
      country: dto.country,
      zip: dto.zip,
      userId,
    });
  }
}
