import { IsNumber, IsOptional, IsString } from 'class-validator';

export class UpsertAddressDto {
  @IsString()
  country: string;

  @IsString()
  city: string;

  @IsString()
  @IsOptional()
  address1: string;

  @IsString()
  @IsOptional()
  address2: string;

  @IsNumber()
  zip: number;
}
