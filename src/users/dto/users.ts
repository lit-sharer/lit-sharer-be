export class CreateUserDto {
  public name: string;
  public email: string;
  public phoneNumber: string;
  public password: string;
}
