import { Injectable } from '@nestjs/common';
import { Command } from 'nestjs-command';

import { UsersService } from '../users.service';
import { WalletsService } from '../wallets/wallets.service';
import { CreateUserDto } from '../dto/users';

@Injectable()
export class UsersSeeder {
  DEFAULT_WALLET_AMOUNT_ADMIN = 1000;
  DEFAULT_WALLET_AMOUNT_USER = 750;

  protected readonly ADMINS = [
    { name: 'sobotnyk', password: '12345', email: 'sobotnyk@litsharer.com' },
  ] as CreateUserDto[];

  protected readonly USERS = [
    { name: 'eduard-1', password: '12345', email: 'eduard-1@litsharer.com' },
    { name: 'eduard-2', password: '12345', email: 'eduard-2@litsharer.com' },
    { name: 'eduard-3', password: '12345', email: 'eduard-3@litsharer.com' },
    { name: 'eduard-4', password: '12345', email: 'eduard-4@litsharer.com' },
    { name: 'eduard-5', password: '12345', email: 'eduard-5@litsharer.com' },
    { name: 'coach-1', password: '12345', email: 'coach-1@litsharer.com' },
    { name: 'coach-2', password: '12345', email: 'coach-2@litsharer.com' },
    { name: 'coach-3', password: '12345', email: 'coach-3@litsharer.com' },
    { name: 'coach-4', password: '12345', email: 'coach-4@litsharer.com' },
    { name: 'coach-5', password: '12345', email: 'coach-5@litsharer.com' },
  ] as CreateUserDto[];

  constructor(
    private usersService: UsersService,
    private walletsService: WalletsService,
  ) {}

  @Command({
    command: 'seed:users',
    describe: 'seeds users',
  })
  async seed() {
    await Promise.all(
      this.ADMINS.map((user) =>
        this.usersService
          .createAdmin(user)
          .then((entity) =>
            this.walletsService.credit(
              entity.id,
              this.DEFAULT_WALLET_AMOUNT_ADMIN,
            ),
          ),
      ),
    );

    await Promise.all(
      this.USERS.map((user) =>
        this.usersService
          .create(user)
          .then((entity) =>
            this.walletsService.credit(
              entity.id,
              this.DEFAULT_WALLET_AMOUNT_USER,
            ),
          ),
      ),
    );

    console.log('Users seeding process has ended successfully!');
  }
}
