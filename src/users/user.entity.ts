import {
  AfterLoad,
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import * as bcrypt from 'bcrypt';

import { Roles } from './roles';
import { WalletEntity } from './wallets/wallet.entity';
import { TransactionEntity } from '../transactions/transaction.entity';
import { AddressEntity } from './addresses/address.entity';
import { getFileUrl } from '../core/utils/files';

@Entity({ name: 'users' })
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false, length: 128 })
  name: string;

  @Column({ unique: true, length: 256 })
  email: string;

  @Column({ nullable: true, length: 20 })
  phoneNumber: string;

  @Column({ nullable: true, length: 512 })
  avatar: string;

  @Column({ nullable: false, select: false })
  passwordHash: string;

  @Column({ type: 'int', default: Roles.User })
  role: Roles;

  @OneToOne(() => WalletEntity, (wallet: WalletEntity) => wallet.user, {
    cascade: true,
  })
  public wallet: WalletEntity;

  @OneToOne(() => AddressEntity, (address: AddressEntity) => address.user, {
    cascade: true,
  })
  public address: AddressEntity;

  @OneToMany(
    () => TransactionEntity,
    (transaction: TransactionEntity) => transaction.user,
    {
      cascade: true,
    },
  )
  public transactions: TransactionEntity[];

  @BeforeInsert()
  async hashPassword() {
    this.passwordHash = await bcrypt.hash(this.passwordHash, 10);
  }

  @BeforeUpdate()
  async hashUpdatedPassword() {
    if (this.passwordHash) {
      this.passwordHash = await bcrypt.hash(this.passwordHash, 10);
    }
  }

  async comparePassword(attempt: string): Promise<boolean> {
    return await bcrypt.compare(attempt, this.passwordHash);
  }

  // readonly url
  protected avatarUrl: string;

  @AfterLoad()
  getUrl() {
    this.avatarUrl = this.avatar ? getFileUrl(this.avatar) : null;
  }
}
