import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UsersService } from './users.service';
import { UserEntity } from './user.entity';
import { WalletsService } from './wallets/wallets.service';
import { WalletEntity } from './wallets/wallet.entity';
import { UsersSeeder } from './seeds/users.seeder';
import { TransactionsModule } from '../transactions/transactions.module';
import { AddressesService } from './addresses/addresses.service';
import { AddressEntity } from './addresses/address.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserEntity, WalletEntity, AddressEntity]),
    TransactionsModule,
  ],
  providers: [UsersService, WalletsService, UsersSeeder, AddressesService],
  controllers: [],
  exports: [UsersService, WalletsService, AddressesService, TypeOrmModule],
})
export class UsersModule {}
