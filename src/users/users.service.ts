import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions, Repository } from "typeorm";

import { UserEntity } from './user.entity';
import { WalletsService } from './wallets/wallets.service';
import { CreateUserDto } from './dto/users';
import { Roles } from './roles';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity) private usersRepo: Repository<UserEntity>,
    private walletService: WalletsService,
  ) {}

  getByEmail(email: string): Promise<UserEntity> {
    return this.usersRepo.findOne({
      where: { email },
      select: ['id', 'passwordHash'],
    });
  }

  getById(id: number) {
    return this.usersRepo.findOne({
      where: { id },
      relations: ['wallet', 'address'],
      select: ['id', 'name', 'email', 'phoneNumber', 'avatar', 'role'],
    });
  }

  get(options: FindOneOptions<UserEntity>) {
    return this.usersRepo.findOne(options);
  }

  save(entity: UserEntity) {
    return this.usersRepo.save(entity);
  }

  async create({ name, email, password, phoneNumber }: CreateUserDto) {
    return this.insert({
      name,
      email,
      passwordHash: password,
      phoneNumber,
      role: Roles.User,
    });
  }

  async createAdmin({ name, email, password, phoneNumber }: CreateUserDto) {
    return this.insert({
      name,
      email,
      passwordHash: password,
      phoneNumber,
      role: Roles.Admin,
    });
  }

  update(id, data: Partial<UserEntity>) {
    return this.usersRepo.update({ id }, data);
  }

  private async insert(data: Partial<UserEntity>) {
    const userEntity = this.usersRepo.create(data);

    await this.usersRepo.insert(userEntity);
    await this.walletService.create(userEntity);

    return userEntity;
  }
}
