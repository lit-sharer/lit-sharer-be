import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Transform } from 'class-transformer';
import { UserEntity } from '../user.entity';

@Entity({ name: 'wallets' })
export class WalletEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Transform(({ value }) => Number(value))
  balance: number;

  @Column()
  @Transform(({ value }) => Number(value))
  blockedBalance: number;

  @Column()
  userId: number;

  @OneToOne(() => UserEntity)
  @JoinColumn()
  public user: UserEntity;
}
