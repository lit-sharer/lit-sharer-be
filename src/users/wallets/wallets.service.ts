import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { WalletEntity } from './wallet.entity';
import { UserEntity } from '../user.entity';
import { TransactionsService } from '../../transactions/transactions.service';

@Injectable()
export class WalletsService {
  constructor(
    @InjectRepository(WalletEntity)
    private walletRepo: Repository<WalletEntity>,
    private transactionsService: TransactionsService,
  ) {}

  async get(userId: number): Promise<WalletEntity> {
    const wallet = await this.walletRepo.findOne({ where: { userId } });

    if (!wallet) {
      throw new NotFoundException('Wallet has not been found!');
    }

    return wallet;
  }

  async create(user: UserEntity) {
    const wallet = this.walletRepo.create({ user });

    return await this.walletRepo.insert(wallet);
  }

  async credit(userId: number, amount: number, comment?: string) {
    await this.transactionsService.createCreditBySystem(
      userId,
      Number(amount),
      comment,
    );
    await this.walletRepo.increment({ userId }, 'balance', Number(amount));
  }

  async creditByCard(userId: number, amount: number) {
    await this.transactionsService.createCreditByUser(
      userId,
      Number(amount),
      'Your balance was increased by using credit card',
    );
    await this.walletRepo.increment({ userId }, 'balance', Number(amount));
  }

  async charge(userId: number, amount: number, comment?: string) {
    await this.transactionsService.createChargeBySystem(
      userId,
      Number(amount),
      comment,
    );
    await this.walletRepo.decrement({ userId }, 'balance', Number(amount));
  }

  async chargeBlocked(userId: number, amount: number, comment?: string) {
    await this.transactionsService.createChargeBySystem(
      userId,
      Number(amount),
      comment,
    );
    await this.walletRepo.decrement({ userId }, 'blockedBalance', Number(amount));
  }

  async containsAmount(userId: number, amount: number): Promise<boolean> {
    const wallet = await this.get(userId);

    if (Number(wallet.balance) < Number(amount)) {
      throw new ForbiddenException(
        'You have not enough money on your account!',
      );
    }

    return true;
  }

  async blockAmount(userId: number, amount: number) {
    const wallet = await this.get(userId);

    return this.walletRepo.update(
      { userId },
      {
        balance: Number(wallet.balance) - Number(amount),
        blockedBalance: Number(wallet.blockedBalance) + Number(amount),
      },
    );
  }

  async unblockAmount(userId: number, amount: number) {
    const wallet = await this.get(userId);

    return this.walletRepo.update(
      { userId },
      {
        balance: Number(wallet.balance) + Number(amount),
        blockedBalance: Number(wallet.blockedBalance) - Number(amount),
      },
    );
  }
}
